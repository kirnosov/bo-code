module matelem
!Module matelem contains subroutines for computing 
!matrix elements with shifted sigma Gaussians 

!Symmetry adaption is applied to the ket using 
!permutation matrices Glob_YHYMatr(:,:,1:Glob_NumYHYTerms)
!
!Input:     
!   vechLk, vechLl :: Arrays of length (n(n+1)/2+n) of exponential parameters
!                                                                  and shifts. 
!   P   :: The symmetry permutation matrix of size n x n
!   grad_k, grad_l :: Gradient flags
!   grad_k=.true.  means that dHkldvechLk, dSkldvechLk need to be computed. 
!   grad_l=.true.  means that dHkldvechLl, dSkldvechLl need to be computed.
!Output:
!   Hkl  ::     Hamiltonian term (normalized)
!   Skl  ::     Overlap matrix element (normalized) 
!   Dk,Dl:: derivatives of normalized Hkl and Skl wrt Paramk
!           and Paraml respectively. They are ordered in the 
!           following manner:
!           Dk=(dHkldvechLk,dSkldvechLk)
!           Dl=(dHkldvechLl,dSkldvechLl)

use globvars
implicit none

contains

subroutine MatrixElements(vechLk, vechLl, P, &
               Hkl, Skl, Dk, Dl, grad_k, grad_l)

real(dprec),intent(in)      :: vechLk(Glob_npt), vechLl(Glob_npt)
real(dprec),intent(in)      :: P(Glob_n,Glob_n)
real(dprec),intent(out)     :: Skl,Hkl
real(dprec),intent(out)     :: Dk(2*Glob_npt),Dl(2*Glob_npt)
logical,intent(in)          :: grad_k, grad_l

!Parameters (These are needed to declare static arrays. Using static 
!arrays makes the function call a little faster in comparison with 
!the case when arrays are dynamically allocated in stack)
integer,parameter :: nn=Glob_MaxAllowedNumOfPseudoParticles
integer,parameter :: nnp=nn*(nn+1)/2
integer,parameter :: nns=nn

!Local variables
integer           n, np, ns 
integer           i, j, k, indx, ii, jj 
real(dprec)       dHkldvechLk(nnp), dHkldvechLl(nnp)
real(dprec)       dSkldvechLk(nnp), dSkldvechLl(nnp)
real(dprec)       Lk(nn,nn), Ll(nn,nn), inv_Lk(nn,nn), inv_Ll(nn,nn)
real(dprec)       Ak(nn,nn), tAl(nn,nn), tAkl(nn,nn), inv_Ak(nn,nn)
real(dprec)       inv_tAkl(nn,nn)!, inv_ttAkl(nn,nn)
real(dprec)       W1(nn,nn), W2(nn,nn)!, W3(nn,nn),W4(nn,nn),W5(nn,nn)
real(dprec)       temp1, temp2, temp3, temp4, temp5, temp6, Z
real(dprec)       det_Lk, det_Ll, det_tAkl,gam
real(dprec)       Tkl, Vkl
real(dprec)       AkAl(nn,nn),AkiAkl(nn,nn), iAklAl(nn,nn)
real(dprec)       AkiAklAk(nn,nn), AliAklAl(nn,nn), AkiAklAl(nn,nn)
real(dprec)       AkAliAkl(nn,nn),iAklAkAl(nn,nn),iAklAlAkiAkl(nn,nn)
real(dprec)       sk(nns),tsl(nns),s(nns)
real(dprec)       s_sl_AlAk(nns),s_sk_Ak(nns),dSkl_dsk(nns),dTkl_dsk(nns)
real(dprec)       dVkl_dsk(nns)
real(dprec)       erfna(nn,NumNuclei),erfer(nn,nn)
real(dprec)       F0na(nn,NumNuclei),F0er(nn,nn)

!write(*,*)'*****************'
!write(*,*)'vechLk = ',vechLk
!write(*,*)'vechLl = ',vechLl

n=Glob_n
np=Glob_np
ns=Glob_ns
!write(*,*)'P='
!write(*,'(<n>(1X,F4.0))')(( P(j,i), j=1,n),i=1,n)

!First we build matrices Lk, Ll, Ak, Al from vechLk, vechLl.
indx=0
do i=1,n
  do j=i,n
    indx=indx+1
        Lk(i,j)=ZERO
        Lk(j,i)=vechLk(indx)
        Ll(i,j)=ZERO
        Ll(j,i)=vechLl(indx)
  enddo
enddo
do i=1,n
  do j=i,n
    temp1=ZERO
        do k=1,j
          temp1=temp1+Lk(i,k)*Lk(j,k)
        enddo
        Ak(i,j)=temp1
        Ak(j,i)=temp1
    temp1=ZERO
        do k=1,j
          temp1=temp1+Ll(i,k)*Ll(j,k)
        enddo
        tAl(i,j)=temp1
        tAl(j,i)=temp1
  enddo
enddo

!build shift vectors as well
sk(1:n)=vechLk(np+1:Glob_npt)
do i=1,n
 temp1=ZERO
 do j=1,n
  temp1=temp1+vechLl(np+j)*P(j,i)
 enddo
 tsl(i)=temp1
enddo

!Then we permute elements of Al to account for 
!the action of the permutation matrix
!tAl=P'*Al*P
!We also form matrix tAkl=Ak+tAl
do i=1,n
  do j=1,n
        temp1=ZERO
    do k=1,n
       temp1=temp1+P(k,j)*tAl(k,i)
        enddo
        W1(j,i)=temp1
  enddo
enddo
do i=1,n
  do j=i,n
    temp1=ZERO
    do k=1,n
           temp1=temp1+W1(i,k)*P(k,j)
        enddo
        tAl(i,j)=temp1
        tAl(j,i)=temp1
        tAkl(i,j)=Ak(i,j)+temp1
        tAkl(j,i)=tAkl(i,j)
  enddo
enddo
!The determinants of Lk and Ll are just
!the products of their diagonal elements
det_Lk=ONE
det_Ll=ONE
do i=1,n
  det_Lk=det_Lk*Lk(i,i)
  det_Ll=det_Ll*Ll(i,i)
enddo

!After this we can do Cholesky factorization of tAkl.
!The Cholesky factor will be temporarily stored in the 
!lower triangle of W1
det_tAkl=ONE
do i=1,n
  do j=i,n
    temp1=tAkl(i,j)
    do k=i-1,1,-1
      temp1=temp1-W1(i,k)*W1(j,k)
    enddo
    if (i==j) then
      W1(i,i)=sqrt(temp1)
      det_tAkl=det_tAkl*temp1
    else
      W1(j,i)=temp1/W1(i,i)
      W1(i,j)=ZERO
    endif
  enddo
enddo

!Inverting tAkl using its Cholesky factor (stored in W1)
!and placing the result into inv_tAkl
do i=1,n
  W1(i,i)=ONE/W1(i,i)
  do j=i+1,n
    temp1=ZERO
    do k=i,j-1
      temp1=temp1-W1(j,k)*W1(k,i)
    enddo
    W1(j,i)=temp1/W1(j,j)
  enddo
enddo

do i=1,n
  do j=i,n
     temp1=ZERO
     do k=j,n
       temp1=temp1+W1(k,i)*W1(k,j)
     enddo
     inv_tAkl(i,j)=temp1
         inv_tAkl(j,i)=temp1
   enddo
enddo

!write(*,*)'Ak='
!write(*,'(<n>(1X,E12.5E2))')(( Ak(j,i), j=1,n),i=1,n)
!write(*,*)'tAl='
!write(*,'(<n>(1X,E12.5E2))')(( tAl(j,i), j=1,n),i=1,n)
!write(*,*)'sk='
!write(*,'(<n>(1X,E12.5E2))')( sk(j), j=1,n)
!write(*,*)'tsl='
!write(*,'(<n>(1X,E12.5E2))')( tsl(j), j=1,n)

!***** start overlap *****!
!Build Ak.iAkl and iAkl.Al
do j=1,n
  do i=1,n
   temp1 = ZERO
   temp2 = ZERO
   temp3 = ZERO
   do k=1,n
      temp1 = temp1 + Ak(i,k) * inv_tAkl(k,j)
      temp2 = temp2 + inv_tAkl(i,k) * tAl(k,j)
      temp3 = temp3 + Ak(i,k) * tAl(k,j)
   enddo
   AkiAkl(i,j) = temp1
   iAklAl(i,j) = temp2
   AkAl(i,j) = temp3
  enddo
enddo
!Build AkiAklAk, AliAklAl and AkiAklAl
do j=1,n
  do i=1,n
   temp1 = ZERO
   temp2 = ZERO
   temp3 = ZERO
   do k=1,n
      temp1 = temp1 + AkiAkl(i,k) * Ak(k,j)
      temp2 = temp2 + AkiAkl(i,k) * tAl(k,j)
      temp3 = temp3 + tAl(i,k) * iAklAl(k,j)
   enddo
   AkiAklAk(i,j) = temp1
   AkiAklAl(i,j) = temp2
   AliAklAl(i,j) = temp3
  enddo
enddo

gam=ZERO
do i=1,n
 temp1=ZERO
 temp2=ZERO
 temp3=ZERO
 do j=1,n
  temp1=temp1+sk(j)*(AkiAklAk(i,j)-Ak(i,j))
  temp2=temp2+tsl(j)*(AliAklAl(i,j)-tAl(i,j))
  temp3=temp3+sk(j)*AkiAklAl(i,j)
 enddo
 gam=gam+temp1*sk(i)+temp2*tsl(i)+TWO*temp3*tsl(i)
enddo

Skl=Glob_2raised3n2*abs(det_Ll*det_Lk)/det_tAkl*&
    sqrt(abs(det_Ll*det_Lk)/det_tAkl)*exp(gam)


!***** start kinetic *****!
! building s=iAkl.(Ak.sk+Al.sl)
do i=1,ns
 s(i)=sk(1)*AkiAkl(1,i)+tsl(1)*iAklAl(i,1)
 do j=2,ns
  s(i)=s(i)+sk(j)*AkiAkl(j,i)+tsl(j)*iAklAl(i,j)
 enddo
enddo

!write(*,*)'s='
!write(*,'(<n>(1X,E12.5E2))')( s(j), j=1,n)

!calculating 2(s-sk).Ak.Al.(s-sl)
! and storing in Tkl
Tkl=ZERO
do i=1,n
 temp1=(s(1)-sk(1))*AkAl(1,i)
 do j=2,n
  temp1=temp1+(s(j)-sk(j))*AkAl(j,i)
 enddo
 Tkl=Tkl+temp1*(s(i)-tsl(i))
enddo
Tkl=TWO*Tkl

!calculating tr[Ak.iAkl.Al]
! and storing in temp1
temp1=ZERO
do i=1,n
 do j=1,n
  temp1=temp1+AkiAkl(i,j)*tAl(j,i)
 enddo
enddo

Tkl=Tkl+THREE*temp1
!write(*,*)'S=',Skl
!write(*,*)'T=',Tkl*Skl
!***** start potential *****!
Vkl=ZERO
do i=1,n
do j=1,NumNuclei
  F0na(i,j)=F0((s(i)-Glob_NucCoord(j))*(s(i)-Glob_NucCoord(j))/(inv_tAkl(i,i)))
  Vkl=Vkl-Glob_NucCharge(j)*&
           F0na(i,j)*TWO*ONEoSQRTPI/sqrt(inv_tAkl(i,i))

 erfna(i,j)=erf(abs(s(i)-Glob_NucCoord(j))/sqrt(inv_tAkl(i,i)))
! Vkl=Vkl-Glob_NucCharge(j)/abs(s(i)-Glob_NucCoord(j))*erfna(i,j)
enddo
enddo
!write(*,*)'A=',Vkl*Skl
do i=1,n
do j=i+1,n
 erfer(i,j)=erf(abs(s(i)-s(j))/sqrt(inv_tAkl(i,i)+inv_tAkl(j,j)-TWO*inv_tAkl(i,j)))
! Vkl=Vkl+ONE/abs(s(i)-s(j))*erfer(i,j)
  F0er(i,j)=F0((s(i)-s(j))*(s(i)-s(j))/(inv_tAkl(i,i)+inv_tAkl(j,j)-TWO*inv_tAkl(i,j)))
  Vkl=Vkl+F0er(i,j)*TWO*ONEoSQRTPI/sqrt(inv_tAkl(i,i)+inv_tAkl(j,j)-TWO*inv_tAkl(i,j))
enddo
enddo
! total hamiltonian
Hkl=(Tkl+Vkl)*Skl+Glob_NucRep*Skl

!write(*,*)'S=',Skl
!stop
! ********** gradients *********!
! gradient _ k
if(grad_k) then
!*** overlap gradient ***!
!calculating 
! s_sk_Ak=(s-sk).Ak
! s_sl_AlAk=(s-sl).Al.Ak
do i=1,ns
 temp1=(s(1)-sk(1))*Ak(1,i)
 temp2=(s(1)-tsl(1))*AkAl(i,1)
 do j=2,ns
  temp1=temp1+(s(j)-sk(j))*Ak(j,i)
  temp2=temp2+(s(j)-tsl(j))*AkAl(i,j)
 enddo
 s_sk_Ak(i)=temp1
 s_sl_AlAk(i)=temp2
enddo

! dSkl/dsk
dSkl_dsk=TWO*Skl*s_sk_Ak

!Inverting Lk. The inverse of a lower triangular matrix is a lower
!triangular matrix. The inverse of Lk will be stored in inv_Lk. The upper
!triangle of inv_Lk is set to be zero.
do i=1,n
  do j=i,n
    inv_Lk(j,i)=Lk(j,i)
  enddo
enddo
do i=1,n
  inv_Lk(i,i)=1/inv_Lk(i,i)
  do j=i+1,n
        inv_Lk(i,j)=ZERO
    temp1=ZERO
    do k=i,j-1
      temp1=temp1-inv_Lk(j,k)*inv_Lk(k,i)
    enddo
    inv_Lk(j,i)=temp1/inv_Lk(j,j)
  enddo
enddo
! computing inv_Ak = inv_Lk'.inv_Lk
do j=1,n
do i=1,n
  inv_Ak(i,j)=ZERO
  do k=1,n
     inv_Ak(i,j)=inv_Ak(i,j)+inv_Lk(k,i)*inv_Lk(k,j) 
  enddo
enddo
enddo

indx=1
do j=1,n
do i=j,n
  dSkldvechLk(indx)=zero
  do k=1,n
     dSkldvechLk(indx) = dSkldvechLk(indx) +& 
        (THREEHALF*inv_Ak(i,k)-THREE*inv_tAkl(i,k)+TWO*&
         (sk(i)*s(k)+sk(k)*s(i)-sk(i)*sk(k)-s(i)*s(k)))*&
        Lk(k,j)
  enddo
  indx=indx+1
enddo
enddo
dSkldvechLk=dSkldvechLk*Skl

!*** kinetic energy gradient ***!
!calculating 
! (s-sk).Ak.Al.invAkl.Ak
! +(s-sl).Al.Ak.invAkl.Ak
! -(s-sl).Al.Ak
do i=1,ns
 temp1=s_sk_Ak(1)*AkiAklAl(i,1)
 temp2=s_sl_AlAk(1)*AkiAkl(i,1)
 do j=2,ns
  temp1=temp1+s_sk_Ak(j)*AkiAklAl(i,j)
  temp2=temp2+s_sl_AlAk(j)*AkiAkl(i,j)
 enddo
 dTkl_dsk(i)=temp1+temp2-s_sl_AlAk(i)
enddo
! dTkl/dsk
dTkl_dsk=Tkl*dSkl_dsk+TWO*Skl*dTkl_dsk

! building Ak.Al.invAkl, invAkl.Ak.Al. and
!                    invAkl.Al.Ak.invAkl
do j=1,n
do i=1,n
 temp1=AkAl(i,1)*inv_tAkl(1,j)
 temp2=inv_tAkl(i,1)*AkAl(1,j)
 temp3=iAklAl(i,1)*AkiAkl(1,j)
 do k=2,n
  temp1=temp1+AkAl(i,k)*inv_tAkl(k,j)
  temp2=temp2+inv_tAkl(i,k)*AkAl(k,j)
  temp3=temp3+iAklAl(i,k)*AkiAkl(k,j)
 enddo
 AkAliAkl(i,j)=temp1
 iAklAkAl(i,j)=temp2
 iAklAlAkiAkl(i,j)=temp3
enddo
enddo
!use temporary matrices W1 and W2 for
! W1=(Al-invAkl.Ak.Al).(s_o_s-sl_o_s-s_o_sk+sl_o_sk)
! W2=(s_o_s-s_o_sk-sk_o_s+sk_o_sk).Ak.Al.invAkl
do j=1,n
do i=1,n
 temp1=zero
 temp2=zero
 do k=1,n
  temp1=temp1+(tAl(i,k)-iAklAkAl(i,k))*&
              (s(k)*s(j)-tsl(k)*s(j)-sk(j)*s(k)+tsl(k)*sk(j))
  temp2=temp2+(s(i)*s(k)-sk(k)*s(i)-sk(i)*s(k)+sk(i)*sk(k))*&
               AkAliAkl(k,j)
 enddo
 W1(i,j)=temp1
 W2(i,j)=temp2
enddo
enddo
! multiplying by Lk and loading into vech
indx=1
do j=1,n
do i=j,n
  dHkldvechLk(indx)=(W1(i,1)+W1(1,i)-W2(i,1)-W2(1,i)+&
         THREE*(iAklAl(i,1)-iAklAlAkiAkl(i,1)))*&
                     Lk(1,j)
  do k=2,n
     dHkldvechLk(indx) = dHkldvechLk(indx) +&
        (W1(i,k)+W1(k,i)-W2(i,k)-W2(k,i)+&
         THREE*(iAklAl(i,k)-iAklAlAkiAkl(i,k)))*&
        Lk(k,j)
  enddo
  indx=indx+1
enddo
enddo
! dHkldvechLk now contains dTkldvechLk
dHkldvechLk=dHkldvechLk*Skl*TWO


!*** potential energy gradient ***
dVkl_dsk=ZERO
! nuclear attraction gradient
do jj=1,NumNuclei
do ii=1,n
 temp1=(s(ii)-Glob_NucCoord(jj))*(s(ii)-Glob_NucCoord(jj)) 
 temp2=inv_tAkl(ii,ii) 
! temp3=dF0(temp1/temp2,F0na(ii,jj))
! write(*,*)'temp3=',temp3
 temp3 = (ONEHALF/abs(temp1/temp2))*&
         (exp(-temp1/temp2)-&
          F0na(ii,jj))
! write(*,*)'temp3=',temp3
! temp3 = (ONEHALF/abs(temp1/temp2))*&
!         (exp(-temp1/temp2)-&
!          ONEHALF*sqrt(pi/abs(temp1/temp2))*erfna(ii,jj))
! write(*,*)'temp3=',temp3

 ! --- wrt s_k
 temp4=Glob_NucCharge(jj)*TWO*ONEoSQRTPI*Skl/temp2/sqrt(temp2)*&
          temp3*TWO*(s(ii)-Glob_NucCoord(jj))
 do indx=1,ns
 dVkl_dsk(indx)=dVkl_dsk(indx)-temp4*AkiAkl(indx,ii)
 enddo

 ! --- wrt L_k
  !use temporary matrices to store
  ! W1=(sk_o_(s-t)).Jii.invAkl-(s_o_(s-t)).Jii.invAkl
  ! W2=invAkl.Jii.invAkl
  do j=1,n
  do i=1,n
   W1(i,j)=(sk(i)-s(i))*(s(ii)-Glob_NucCoord(jj))*inv_tAkl(ii,j)
   W2(i,j)=inv_tAkl(i,ii)*inv_tAkl(ii,j)
  enddo
  enddo
  ! multiplying by Lk and loading into vech
  temp4=Skl*erfna(ii,jj)/(sqrt(temp1)*temp2)
  temp5=FOUR*Skl*ONEoSQRTPI*temp3/sqrt(temp2*temp2*temp2)
  temp6=-temp5*temp1/temp2
  indx=1
  do j=1,n
  do i=j,n
    do k=1,n
       dHkldvechLk(indx) = dHkldvechLk(indx) - Glob_NucCharge(jj)*&
          (temp5*(W1(i,k)+W1(k,i)) + (temp4-temp6)*W2(k,i) )*Lk(k,j)
    enddo
    indx=indx+1
  enddo
  enddo
!write(*,*)'dVkldvechLk='
!write(*,'(<n>(1X,E11.5E1))')( dVkldvechLk(j), j=1,n*(n+1)/2)
enddo!ii
enddo!jj

!write(*,*)'dVkldvechLk='
!write(*,'(<n>(1X,E11.5E1))')( dVkldvechLk(j), j=1,n*(n+1)/2)


! electron repulsion gradient
do ii=1,n
do jj=ii+1,n
 temp1=(s(ii)-s(jj))*(s(ii)-s(jj))
 temp2=inv_tAkl(ii,ii)-TWO*inv_tAkl(ii,jj)+inv_tAkl(jj,jj)
 Z=abs(temp1/temp2)
if(Z.lt.Glob_dF0_cutoff) then
  temp3=-1.0d0/3.0d0 + Z/5.0d0 - Z*Z/14.0d0 + &
         Z*Z*Z/54.0d0 - Z*Z*Z*Z/264.0d0 + &
         Z*Z*Z*Z*Z/1560.0d0
else
  temp3 = (ONEHALF/Z)*(exp(-Z)-F0er(ii,jj))
endif
!write(*,*)'temp1 = ',temp1
!write(*,*)'temp2 = ',temp2
! write(*,*)'temp3 = ',temp3

 ! *** wrt s_k
 do indx=1,ns
 dVkl_dsk(indx)=dVkl_dsk(indx)+&
               TWO*ONEoSQRTPI*Skl/temp2/sqrt(temp2)*temp3*&
               TWO*(AkiAkl(indx,ii)-AkiAkl(indx,jj))*(s(ii)-s(jj))
 enddo
 ! *** wrt L_k
   !use temporary matrices to store
  ! W1=(sk_o_s).Jij.invAkl-(s_o_s).Jij.invAkl
  ! W2=invAkl.Jij.invAkl
  do j=1,n
  do i=1,n
   W1(i,j)=inv_tAkl(ii,j)*(sk(i)-s(i))*s(ii)-inv_tAkl(jj,j)*(sk(i)-s(i))*s(ii)-&
           inv_tAkl(ii,j)*(sk(i)-s(i))*s(jj)+inv_tAkl(jj,j)*(sk(i)-s(i))*s(jj)
   W2(i,j)=inv_tAkl(ii,j)*inv_tAkl(i,ii)-inv_tAkl(jj,j)*inv_tAkl(i,ii)-&
           inv_tAkl(ii,j)*inv_tAkl(i,jj)+inv_tAkl(jj,j)*inv_tAkl(i,jj)
  enddo
  enddo
  ! multiplying by Lk and loading into vech
if(Z.lt.1.0d-50) then
  temp4=Skl*ONEoSQRTPI/(sqrt(temp2)*temp2)
else
  temp4=Skl*erfer(ii,jj)/(sqrt(temp1)*temp2) 
endif
!  temp4=Skl*erfer(ii,jj)/(sqrt(temp1)*temp2)
!  write(*,*)'temp4 = ',temp4
  temp5=FOUR*Skl*ONEoSQRTPI*temp3/sqrt(temp2*temp2*temp2)
!  write(*,*)'temp5 = ',temp5
  temp6=-temp5*temp1/temp2
  indx=1
  do j=1,n
  do i=j,n
    do k=1,n
       dHkldvechLk(indx) = dHkldvechLk(indx) +&
          (temp5*(W1(i,k)+W1(k,i)) + (temp4-temp6)*W2(k,i) )*Lk(k,j)
    enddo
    indx=indx+1
  enddo
  enddo
!write(*,*)'dVkldvechLk='
!write(*,'(<n>(1X,E11.5E1))')( dVkldvechLk(j), j=1,n*(n+1)/2)
!stop
enddo!ii
enddo!jj

dVkl_dsk=dVkl_dsk+(Vkl)*dSkl_dsk
! potential energy gradient _ sk

dHkldvechLk=dHkldvechLk+(Tkl+Vkl+Glob_NucRep)*dSkldvechLk
! total energy gradient _ Lk

!write(*,*)'dHkldvechLk = ',dHkldvechLk
!write(*,*)'dVkl_dsk = ',dVkl_dsk

Dk(1:np)=dHkldvechLk(1:np)
Dk(np+1:np+ns)=dVkl_dsk(1:ns)+dTkl_dsk(1:ns)+&
               Glob_NucRep*dSkl_dsk(1:ns)
Dk(Glob_npt+1:Glob_npt+np)=dSkldvechLk(1:np)
Dk(Glob_npt+np+1:Glob_npt+np+ns)=dSkl_dsk(1:ns)

!write(*,*)'Dk='
!write(*,'(<2*Glob_npt>(1X,E11.5E1))')( Dk(j), j=1,2*Glob_npt)
endif!grad_k

Dl=ZERO
!write(*,*)'*****************'

end subroutine MatrixElements

FUNCTION F0(Z)

  implicit none
  real(dprec) :: Z,F0

Z=abs(Z)
!write(*,*)'z=',z
if (z.gt.1.0d-50) then
 F0=(SQRTPI/sqrt(Z))*erf(sqrt(Z))*ONEHALF
else
 F0=ONE
endif

END FUNCTION F0

FUNCTION dF0(Z,f)

  implicit none
  real(8) :: Z,f,dF0

Z=abs(Z)

if (Z.lt.1.3099d-2) then
 dF0=-ONETHIRD + Z*ONEFIFTH - Z*Z/14.0d0 + Z*Z*Z/54.0d0 - Z*Z*Z*Z/264.0d0 +&
      Z*Z*Z*Z*Z/1560.0d0
else
dF0 = (ONEHALF/z)*(exp(-Z)-f)
endif

END FUNCTION dF0


end module matelem
