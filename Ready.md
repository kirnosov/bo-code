# Born-Oppenheimer Approximation Code

## Code structure:

- *globvars.f90* contains global variables and parameters definitions 

- *basicdef_\*.f90* contain numerical precision info and i/o methods

- *\*.f* files are included to ensure code portability; machine-optimized 
standard libraries should be used for better performance. 

- *inout.txt* is a calculation parameters input file, which will be read 
and modified by the code.

- *linalg.f90* contains all the linear algebra subroutines

- *main.f90* calls program initialization subroutines and executes the
optimization program

- *matform.f90* module contains procedures that form Hamiltonian and 
overlap matrices and related routines

- *matelem.f90* module contains subroutines for computing matrix elements 
with shifted sigma Gaussians