F90 = mpiifort
F77 = mpiifort
FCFLAGS=-O3 
LINKFLAGS=-O3 
#FCFLAGS=-O3 -ipo -xhost
#LINKFLAGS=-O3 -ipo -xhost
FILES = basicdef_8.f90 X1MACH.f90 globvars.f90 misc.f90 dmng_8.f linalg.f90 matelem.f90 matform.f90 workproc.f90 main.f90 LPKBLS_8.f
OBJS =  basicdef_8.o   X1MACH.o globvars.o misc.o dmng_8.o linalg.o matelem.o matform.o workproc.o main.o LPKBLS_8.o

main	 : $(OBJS)
	$(F90) $(LINKFLAGS) $(OBJS) -o main

basicdef_8.o	:	basicdef_8.f90
	$(F90) $(FCFLAGS) -c basicdef_8.f90 -o basicdef_8.o

globvars.o : globvars.f90 basicdef_8.o
	$(F90) $(FCFLAGS) -c globvars.f90 -o globvars.o

misc.o : misc.f90 globvars.o
	$(F90) $(FCFLAGS) -c misc.f90 -o misc.o

dmng_8.o : dmng_8.f 
	$(F77) $(FCFLAGS) -c dmng_8.f -o dmng_8.o 

LPKBLS_8.o : LPKBLS_8.f
	$(F90) $(FCFLAGS) -c LPKBLS_8.f -o LPKBLS_8.o

X1MACH.o : X1MACH.f90
	$(F90) $(FCFLAGS) -c X1MACH.f90 -o X1MACH.o

linalg.o : linalg.f90 globvars.o
	$(F90) $(FCFLAGS) -c linalg.f90 -o linalg.o

matelem.o : matelem.f90 globvars.o
	$(F90) $(FCFLAGS) -c matelem.f90 -o matelem.o 

matform.o : matform.f90 matelem.o 
	$(F90) $(FCFLAGS) -c matform.f90 -o matform.o

workproc.o : workproc.f90 linalg.o matform.o X1MACH.o LPKBLS_8.o dmng_8.o
	$(F90) $(FCFLAGS) -c workproc.f90 -o workproc.o 

main.o	 : main.f90 workproc.o
	$(F90) $(FCFLAGS) -c main.f90 -o main.o 

clean	:
	rm -rf	*.o	main	*.mod	*~	*.PT	*.out

